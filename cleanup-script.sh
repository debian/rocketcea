#!/bin/sh
#
#      clean upstream source to achieve DFSG compliance
#      Copyright 2024 by Bdale Garbee.  GPL v3 or any later version.
#

# remove backup file that interferes with building cleanly
rm -f cea_obj.py.bak backup/cea_obj.py.bak

# remove odd zip file file that doesn't seem to be used
rm -f backup/RocketCEA\ Install.flash.zip.html

# remove pre-run coverage analysis output to avoid copyright assertion confusion
rm -rf cover 

# remove pre-built sphinx doc tree
rm -rf docs/_build/* rocketcea/sphinx_html

# remove generated .csv files that litter delivered python dir
find . -name separated_Noz.csv -exec rm {} \;

# remove pre-built Windows binaries
find . -name \*.exe -exec rm {} \;

# remove embedded jquery and related javascript droppings
rm -rf rocketcea/sphinx_html/_static/jquery*
find . -name \*.pyd -exec rm {} \;
find . -name \*.js -exec rm {} \;

